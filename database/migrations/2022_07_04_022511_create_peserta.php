<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeserta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta', function (Blueprint $table) {
            $table->id();
            $table->string('no_ktp',16)->nullable('false');
            $table->string('nama',150)->nullable();
            $table->string('jenis_kelamin',10)->nullable();
            $table->string('tempat_lahir',50)->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('status_pernikahan',20)->nullable();
            $table->string('no_hp',50)->nullable();
            $table->string('npwp',50)->nullable();
            $table->string('kewarganegaraan',100)->nullable();
            $table->string('kelas',50)->nullable();
            $table->string('email',100)->nullable();
            $table->string('alamat',250)->nullable();
            $table->string('rt',2)->nullable();
            $table->string('rw',2)->nullable();
            $table->string('kode_pos',5)->nullable();
            $table->string('no_telp_rumah',50)->nullable();
            $table->string('kelurahan',100)->nullable();
            $table->string('no_kk',16)->nullable();
            $table->string('status_keluarga',100)->nullable();
            $table->integer('jmlh_anak')->length(2)->nullable();
            $table->string('rekening',50)->nullable();
            $table->string('no_rekening',50)->nullable();
            $table->string('nama_pemilik_rekening',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peserta');
    }
}
