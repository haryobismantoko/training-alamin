<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\PesertaController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
});


require __DIR__ . '/auth.php';

// Route::get('/product',[ProductController::class,'index']);
// Route::post('/product',[ProductController::class,'store']);
// Route::get('/product/{product}/edit',[ProductController::class,'edit']);
// Route::put('/product/{product}/put',[ProductController::class,'update']);

Route::resource('product',ProductController::class);
Route::resource('peserta',PesertaController::class);
// Route::get('peserta/{id}', [Pro])

// Route::get('/peserta',[PesertaController::class,'index']);
// Route::post('/peserta',[PesertaController::class,'store']);
// Route::get('/peserta/create',[PesertaController::class,'create'])->name('peserta.create');
// Route::get('/peserta/{peserta}/edit',[PesertaController::class,'edit']);
// Route::put('/peserta/{peserta}/put',[PesertaController::class,'update']);
