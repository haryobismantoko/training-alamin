<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePesertaRequest;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Peserta;
use Illuminate\Support\Facades\Redirect;

class PesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $peserta = Peserta::all();
        return Inertia::render('PesertaTampil', compact('peserta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Peserta');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePesertaRequest $request)
    {
        
        Peserta::create($request->all());
        // $input = $request->all();
        // Peserta::create($input);

        return redirect ('/peserta')
        ->with([
            'message'=>'Data Tersimpan',
            'type'=>'info,'
    ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Peserta $peserta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $peserta = Peserta::where('id', $id)->first();
        return Inertia::render('Peserta',['peserta' => $peserta]);
        // return response()->json([
        //     'peserta' => $peserta,
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePesertaRequest $request, $id)
    {
        $peserta = Peserta::findOrFail($id);
        // $peserta->fill($request->all())->save();
        $peserta->update($request->all());
        return Redirect::route("peserta.index")->with([
            'message'=>'Data Tersimpan',
            'type'=>'info,'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $peserta = Peserta::findOrfail($id);    
        $peserta->delete();   
        return Redirect::route('peserta.index')
        ->with([
            'message'=> 'Peserta Berhasil dihapus!!',
            'type' => 'success'
        ]);
       

    }
}
