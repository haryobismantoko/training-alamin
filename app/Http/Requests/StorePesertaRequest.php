<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePesertaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      
        return [
            'no_ktp'=>'required|max:16',
            'nama'=>'max:150',
            'jenis_kelamin'=>'max:10',
            'tempat_lahir'=>'max:50',
            'tgl_lahir',
            'status_pernikahan'=>'max:20',
            'no_hp'=>'max:50',
            'npwp'=>'max:50',
            'kewarganegaraan'=>'max:100',
            'kelas'=>'max:50',
            'email'=>'max:100',
            'alamat'=>'max:250',
            'rt'=>'max:2',
            'rw'=>'max:2',
            'kode_pos'=>'max:5',
            'no_telp_rumah'=>'max:50',
            'kelurahan'=>'max:100',
            'no_kk'=>'max:16',
            'status_keluarga'=>'max:100',
            'jmlh_anak'=>'max:2',
            'no_rekening'=>'max:50',
            'nama_pemilik_rekening'=>'max:100',
        ];
    }
}
